<?php

namespace App\Http\Controllers;

class GuestController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function unavailable()
    {
        return view('unavailable');
    }
}
