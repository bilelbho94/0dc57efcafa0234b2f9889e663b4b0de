<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Services\ShortLinkService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ShortLinkRedirectController extends Controller
{
    public function handle(String $code, ShortLinkService $service)
    {
        $redirectUrl =  $this->getPath("unavailable");
        $link = $service->get($code);
        if ($link != null) {
            $redirectUrl = $link->link;
        }
        return redirect($redirectUrl);
    }
}
