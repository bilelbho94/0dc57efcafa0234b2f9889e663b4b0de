<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Services\ShortLinkService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ShortLinkController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, ShortLinkService $service)
    {
        $shortLinks = $service->available();
        return view('dashboard', compact('shortLinks'));
    }

    public function generate(Request $request, ShortLinkService $service)
    {
        $request->validate([
            'link' => 'required|url'
        ]);

        try {
            $service->create($request->link);
        } catch (\Exception $ex) {
            return redirect($this->getPath("dashboard"))
                ->with('error', $ex->getMessage());
        }
        return redirect($this->getPath("dashboard"))
            ->with('success', 'Shorten Link Generated Successfully!');
    }

    public function delete(Request $request, ShortLinkService $service)
    {
        $service->delete($request->code);
        return redirect($this->getPath("dashboard"))
            ->with('success', 'Shorten Link Deleted Successfully!');
    }
}
