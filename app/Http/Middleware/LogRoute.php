<?php

namespace App\Http\Middleware;

use App\Services\LoggerService;
use Closure;
use Illuminate\Http\Request;

class LogRoute
{

    private $logger;
    public function __construct(LoggerService $logger)
    {
        $this->logger = $logger;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $this->logger->log($request);
        return $next($request);
    }
}


// Le temps d'accès
// ○ Le lien accédé
// ○ L'identifiant de l'utilisateur s'il est connecté
// ○ Adresse IP du client
// ○ Le pays
// ○ Le User Agent du navigateur