<?php

namespace App\Console\Commands;

use App\Models\Link;
use Illuminate\Console\Command;

class linkExpChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'link:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command check link validity';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $links = Link::whereRaw("created_at <= (NOW() - INTERVAL 1 DAY)")->get();
        foreach ($links as $link) {
            $link->delete();
        }
        return 0;
    }
}
