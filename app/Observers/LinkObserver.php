<?php

namespace App\Observers;

use App\Models\Link;

class LinkObserver
{
    public function creating(Link $link)
    {
        $user_id = $link->user_id;

        $count = Link::where("user_id", $user_id)->count();
        if ($count >= 5) {
            throw new \Exception('Shorten Link could not be created trail limit 5 links delete link and try agin !');
        }

        $old = Link::where("link", $link->link)->where("user_id", $user_id)->first();
        if ($old != null) {
            throw new \Exception('Shorten Link already exist with code `' . $old->code . '`');
        }

        $count = Link::count();
        if ($count == 20) {
            $last =  Link::first();
            if ($last != null) {
                $last->delete();
            }
        }
    }
}
