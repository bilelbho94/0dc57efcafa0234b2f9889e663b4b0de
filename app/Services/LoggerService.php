<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoggerService
{

    private $countryFinder;
    public function __construct(CountryFinderService $countryFinder)
    {
        $this->countryFinder = $countryFinder;
    }

    public function log(Request $request)
    {
        $logInfo = [];
        $logInfo["url"] = $request->getRequestUri();
        $userId = Auth::id();
        if ($userId != null) {
            $logInfo["userId"] = $userId;
        }
        $logInfo["ip"] = $request->ip();
        $logInfo["country"] = $this->countryFinder->find("196.224.38.86");
        $logInfo["userAgent"] = $request->header('User-Agent');
        Log::channel("access")->info('User access', $logInfo);
    }
}
