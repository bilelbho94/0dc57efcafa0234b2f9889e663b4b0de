<?php

namespace App\Services;

use Tuupola\Base58;

class CodeGenerator
{
    public function generate(int $id): string
    {
        $base58 = new Base58();
        $uid = strval($id);
        $len = strlen($uid);
        if ($len < 4) {
            $uid = sprintf("0000%s", $id);
            $uid = substr($uid, -4);
        }
        $uid = $base58->encode($uid);
        return $uid;
    }
}
