<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use GeoIp2\Database\Reader;

class CountryFinderService
{


    public function find(string $ip): string
    {
        $path = Storage::path('GeoLite2-Country_20220315/GeoLite2-Country.mmdb');
        $reader = new Reader($path);

        try {
            $record = $reader->country($ip);
            return $record->country->name;
        } catch (\Exception $ex) {
            //TODO: handle exception
        }
        return "N\A";
    }
}
