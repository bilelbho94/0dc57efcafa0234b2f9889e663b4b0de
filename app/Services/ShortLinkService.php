<?php

namespace App\Services;

use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Tuupola\Base58;

class ShortLinkService
{
    protected $generator;
    
    public function __construct(CodeGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function available(int $id = null)
    {
        if ($id == null) {
            $id = Auth::id();
        }
        $links = Link::where("user_id", $id)->orderByDesc("id")->get();
        return  $links;
    }

    public function create($link): Link
    {
        $shortLink = new Link;
        $nextId = $shortLink->getNextId();
        $code =  $this->generator->generate($nextId);
        $shortLink->link = $link;
        $shortLink->code = $code;
        $shortLink->user_id = Auth::id();
        $shortLink->save();
        return $shortLink;
    }

    public function get($code): ?Link
    {
        $query = Link::where("code", $code);
        $link = $query->first();
        if ($link != null) {
            $link->hits++;
            $link->save();
        }
        return $link;
    }


    public function delete($code)
    {
        $link = $this->get($code);
        if ($link != null) {
            $link->delete();
        }
    }
}
