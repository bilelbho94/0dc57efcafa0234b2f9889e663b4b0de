<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Link extends Model
{
    use HasFactory;

    protected $fillable = [
        "link", "code"
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getNextId()
    {
        $link = Link::orderByDesc("id")->first();
        if ($link != null) {
            return $link->id + 1;
        }
        return 1;
    }
}
