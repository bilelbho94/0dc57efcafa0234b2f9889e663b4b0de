@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center ">
        <div class="col-md-8">
            <h1 class="w-100 text-center">{{__("THIS LINK IS UNAVAILABLE")}}</h1>
            @guest
                @if (Route::has('login'))
                    <a class="d-block text-center" href="{{ route("login",app()->getLocale())}}">Try our cool serivce</a>
                @endif
            @endif
        </div>
    </div>
</div>
@endsection