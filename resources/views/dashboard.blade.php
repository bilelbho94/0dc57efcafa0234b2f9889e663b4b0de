@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <form method="POST" action="{{ route('shorten.link.generate', app()->getLocale()) }}">
                            @csrf
                            <div class="input-group mb-3">
                                <input type="text" name="link" class="form-control" placeholder="Enter URL"
                                    aria-label="Recipient's username" aria-describedby="basic-addon2"
                                    class="@error('link') is-invalid @enderror">

                                <div class="input-group-append">
                                    <button class="btn btn-success" type="submit">Generate Shorten Link</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger">
                                <p>{{ Session::get('error') }}</p>
                            </div>
                        @endif
                        @error('link')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <p>COUNT {{ count($shortLinks) }}/5</p>
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Short Link</th>
                                    <th>Link</th>
                                    <th>Hits</th>
                                    <th>CreatedAt</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($shortLinks as $row)
                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td><a href="{{ route('shorten.link.handle', $row->code) }}"
                                                target="_blank">{{ route('shorten.link.handle', $row->code) }}</a></td>
                                        <td>{{ $row->link }}</td>
                                        <td>{{ $row->hits }}</td>
                                        <td>{{ $row->created_at }}</td>
                                        <td>
                                            <form method="post"
                                                action="{{ route('shorten.link.delete', app()->getLocale()) }}">
                                                @csrf
                                                <input type="hidden" name="code" value="{{ $row->code }}" />
                                                <button type="submit" class="btn btn-sm btn-danger">x</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
