<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect("/", "/en");

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => 'fr|en']
], function () {
    Route::get('/', [App\Http\Controllers\GuestController::class, 'index'])
        ->name('guest.welcome');

    Route::get('/', [App\Http\Controllers\GuestController::class, 'unavailable'])
        ->name('guest.unavailable');

    Auth::routes();

    Route::get('/dashboard', [App\Http\Controllers\ShortLinkController::class, 'index'])
        ->name('shorten.link');

    Route::post('/dashboard', [App\Http\Controllers\ShortLinkController::class, 'generate'])
        ->name('shorten.link.generate');

    Route::post('/dashboard/delete', [App\Http\Controllers\ShortLinkController::class, 'delete'])
        ->name('shorten.link.delete');
});

Route::get('{code}', [App\Http\Controllers\ShortLinkRedirectController::class, 'handle'])
    ->name('shorten.link.handle');
